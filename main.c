#include <stdio.h>
#include <stdlib.h>

int leituraint(void);

//float leiturafloat(void);

//char leiturachar(void);

int mainmenu(void);

int mainmenu(void);

void recuperarjogo(void);

void sobrejogo(void);

void salvarjogo(void);

void novojogo(void);

void comojogar();

void intro();

int escolhapersonagem();

void missaoporto();

void missaoflorestabranca();

void missaoinferno();

void missaoflorestanegra();

void missaolisboa();

void mapa(void);

void saopedro();

void paraiso();

void casteloreia();

void castelodrac();


void objetivo();

void missaoporto_2();

void missaoflorestabranca_2();

void missaoflorestanegra_2();

void missaolisboa_2();

void castelodrac_2();

void carregarjogo();

void historiaporto();

void historialisboa();

void historiaflbranca();

void historiafnegra();

void historiacdrac();

void historiasaopedro();

void historiainferno();

void historiaparaíso();

int nivel = 0;
int personagem;
int objectivo = 0;

int main() {
    int i = -1;
    while (i != 5) {

        i = mainmenu();
        if (i == 5){
            break;
        }
    }


    return 0;
}

void sobrejogo(void) {
    printf("\nAqui temos as informações sobre o Jogo");
    printf("\nEste joguito foi desenvolvido pelo André Ricardo");
    printf("\nÉ um jogo em mundo aberto em que podemos andar por vários locais");
    printf("\nO jogo só acaba quando a pessoa quer ou quando a nossa personagem morre");
    printf("\nO joguito servir como projecto final no CET com o apoio do Júlio Magalhães e de forma\na evoluir o meu C.\n");
    int enter=1;
    while (enter != 0) {
        printf("\nEscreva 0/zero para voltar ao main menu\n");
        enter = leituraint();


    }
    mainmenu();
}



int mainmenu(void) {
    int opcao = 0;
    while (opcao == 0 || opcao < 0 || opcao > 5) {
        printf("Escolha uma das opções abaixo\n");
        printf("1-- NOVO JOGO\n");
        printf("2-- JOGO SALVO\n");
        printf("3-- SOBRE\n");
        printf("4-- COMO JOGAR\n");
        printf("5-- SAIR\n");
        opcao = leituraint();

    }

    switch (opcao) {

        case 1:
            //printf("Novo Jogo");
            novojogo();
            break;
        case 2:
            //printf("Jogo Salvo");
            carregarjogo();
            break;
        case 3:
            sobrejogo();
            break;
        case 4:
            comojogar();
            break;
        case 5:
            return opcao;


    }


}

void carregarjogo() {
    FILE *gravar;
    gravar = fopen("gravar.txt", "r");
    fscanf(gravar, "%d\n%d\n%d", &nivel,&personagem,&objectivo);
    printf("%d\n%d\n%d\n",nivel,personagem,objectivo);
    fclose(gravar);
    mapa();
}

void comojogar() {
    printf("\nEscolher a personagem, temos até agora o Homem , a Mulher e o Velhote");
    printf("\nCada personagem tem caracteristicas diferentes");
    printf("\nE assim probabilidades diferentes nas missões");
    printf("\nHá missões fáceis, medianas e difíceis");
    printf("\nNós começamos no nível 1 e podemos chegar ao 6");
    printf("\nNo nível 6 tornamo-nos quase Deuses tornando as missões básicas e abrindo umas missões especiais");
    printf("\nPodemos morrer caso a missão corra mal e o jogo acaba");
    int enter=1;
    while (enter != 0) {
        printf("\nEscreva 0/zero para voltar o main menu\n");
        enter = leituraint();


    }
    mainmenu();

}

void novojogo() {
    intro();
    int opcao = -1;
    //printf("\n1-- Homem");
    //printf("\n2-- Mulher");
    //printf("\n3-- Velhote");
    personagem = escolhapersonagem();
    while (opcao != 0) {
        if (personagem == 1) {
            printf("\nEscolheste o Homem e começas na cidade do Porto");
            missaoporto();
            opcao == leituraint();

        } else if (personagem == 2) {
            printf("\nEscolheste a Mulher e começas na Floresta Branca");
            printf("\nPersonagem em implementação, escolha outra sff!\n");
            novojogo();
        } else if (personagem == 3) {
            printf("\nEscolheste o Velhote e começas no Mapa\n");
            printf("\nPersonagem especial em implementação, parcialmente implementado, pode navegar para\nO Inferno, paraíso e Sáo Pedro\n");
            mapa();
            opcao == leituraint();
        }/* else if (personagem == 4) {
            printf("\nEscolheste o Velhote e começas na Floresta Negra");
            missaoflorestanegra();
            opcao == leituraint();
        } else {
            printf("\nEscolheste o Político e começas na cidade de Lisboa");
            missaolisboa();
            opcao == leituraint();
        }*/

    }
}

void mapa() {
    int opcao = 0;
    printf("\n\nEstás no mapa mundial e o teu nível é %d ", nivel);
    while (opcao == 0 || opcao < 0 || opcao > 10) {
        printf("\nEscolha um dos locais abaixo\n");
        printf("1-- Cidade do Porto\n");
        printf("2-- Cidade de Lisboa\n");
        printf("3-- Floresta Branca\n");
        printf("4-- Floresta Negra\n");
        printf("5-- Castelo do Drácula\n");
        //printf("6-- Castelo do Rei Artur\n");
        printf("6-- São Pedro\n");
        printf("7-- Inferno\n");
        printf("8-- Paraíso\n");
        printf("9-- Salvar Jogo\n");
        printf("10--SAIR\n");
        opcao = leituraint();
    }
    switch (opcao) {

        case 1:
            //porto
            printf("\nQuer entrar na Cidade do Porto ou ver a sua História?");
            printf("\n1-- Entrar!");
            printf("\n2-- História\n");
            opcao=0;
            while (opcao == 0 || opcao < 0 || opcao > 2) {
                opcao=leituraint();
                if (opcao==1){
                    missaoporto();
                }else
                {
                    historiaporto();
                }
            }
            break;
        case 2:
            //lisboa
            printf("\nQuer entrar na Cidade de Lisboa ou ver a sua História?");
            printf("\n1-- Entrar!");
            printf("\n2-- História\n");
            opcao=0;

            while (opcao == 0 || opcao < 0 || opcao > 2) {
                opcao=leituraint();
                if (opcao==1){
                    missaolisboa();
                }else
                {
                    historialisboa();
                }


            }
            break;

        case 3:
            //fbranca
            printf("\nQuer entrar na Floresta Branca ou ver a sua História?");
            printf("\n1-- Entrar!");
            printf("\n2-- História\n");
            opcao=0;

            while (opcao == 0 || opcao < 0 || opcao > 2) {
                opcao=leituraint();
                if (opcao==1){
                    missaoflorestabranca();
                }else
                {
                    historiaflbranca();
                }


            }
            break;
        case 4:
            //fnegra
            printf("\nQuer entrar na Floresta negra ou ver a sua História?");
            printf("\n1-- Entrar!");
            printf("\n2-- História\n");
            opcao=0;

            while (opcao == 0 || opcao < 0 || opcao > 2) {
                opcao=leituraint();
                if (opcao==1){
                    missaoflorestanegra();
                }else
                {
                    historiafnegra();
                }


            }
            break;
        case 5:
            printf("\nQuer entrar no Castelo do Drácula ou ver a sua História?");
            printf("\n1-- Entrar!");
            printf("\n2-- História\n");
            opcao=0;

            while (opcao == 0 || opcao < 0 || opcao > 2) {
                opcao=leituraint();
                if (opcao==1){
                    castelodrac();
                }else
                {
                    historiacdrac();
                }


            }
            break;
        case 6:
            //spedro
            if(personagem == 1 || personagem ==2){
                printf("\nÉs apenas um Humano não podes ir directo ao São Pedro sem morrer");
                mapa();
            }else
            {
                printf("\nQuer falar com o São Pedro ou ver a sua História?");
                printf("\n1-- Entrar!");
                printf("\n2-- História\n");
                opcao=0;

                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    opcao=leituraint();
                    if (opcao==1){
                        saopedro();
                    }else
                    {
                        historiasaopedro();
                    }


                }
            }
            break;
        case 7:
            //inferno
            if(personagem == 1 || personagem ==2){
                printf("\nÉs apenas um Humano não podes ir directo ao Inferno sem morrer");
                mapa();
            }else {
                printf("\nQuer ir para o Inferno ou ver a sua História?");
                printf("\n1-- Entrar!");
                printf("\n2-- História\n");
                opcao=0;

                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    opcao=leituraint();
                    if (opcao==1){
                        missaoinferno();
                    }else
                    {
                        historiainferno();
                    }


                }

            }
            break;
        case 8:
            //paraiso
            if(personagem == 1 || personagem ==2){
                printf("\nÉs apenas um Humano não podes ir directo ao Paraíso sem morrer");
                mapa();
            }else {
                printf("\nQuer ir para o Paraíso ou ver a sua História?");
                printf("\n1-- Entrar!");
                printf("\n2-- História\n");
                opcao=0;

                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    opcao=leituraint();
                    if (opcao==1){
                        paraiso();
                    }else
                    {
                        historiaparaíso();
                    }


                }
            }
            break;
        case 9:
            salvarjogo();
            break;
        case 10:
            printf("\nQueres mesmo Sair? Se o jogo não estiver gravado perdes tudo\n");
            opcao = 0;
            printf("\n1-- Sim ");
            printf("\n2-- Não\n");
            opcao = leituraint();
            if (opcao ==1){
                mainmenu();
            }
            else{
                mapa();
            }

            break;
    }

}

void historiaparaíso() {
    printf("\nHá pouco conhecimento sobre as origens do paraíso,\n há quem diga que foi o Deus maior que o criou para os seus filhos,\n há quem diga que foi um conjunto de Deuses e há quem diga que já existia no antes do inicio dos tempos sendo algo anterior ao tempo em si.\n O objetivo é dar uma vida eterna ao dignos.\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void historiainferno() {
    printf("\nCriado por deuses para castigar as almas dos piores,\n Deus mandou o seu filho favorito para os castigar,\n sabendo que seria ele o único capaz\n de aguentar tal suplicio durante milénios sem enlouquecer.\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void historiasaopedro() {
    printf("\nO São Pedro questiona as almas dos mortos e decide para irão ser mandadas para \no inferno ou o paraíso ou em certos casos de volta a \nTerra porque não têm lugar nem num lado nem outro.\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void historiacdrac() {
    printf("\nVlad após renegar Deus bebeu o sangue dos seus inimigos e virou uma criatura maligna \nque se alimenta de sangue humano, imortal e quase invencível. \nDrácula ou Vlad ao contrário dos seus descendentes pode caminhar durante o dia ao sol,\n com o crescimento da população de Vampiros foi capaz de conquistar grande parte da antiga Roménia mas para \nisso teve que criar uma quinta enorme de humanos em Lisboa,\n aproveitando o colapso de Portugal.\n O seu Castelo vivo consegue mudar de local a bem entender do seu mestre,\n nunca se sabendo realmente onde se encontra. \nFoi desta forma que conquistou Lisboa e outros territórios. Os objetivos de Vlad são difíceis de entender,\n muitas vezes agressivo outra vezes pacificador e cientifico.\n Todos interessados em ciência são bem vindos ao seu Castelo e tem um certo fascínio por vida extraterrestre e por mágia.\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void historiafnegra() {
    printf("\nAntiga Serra da Arrábida agora ocupada \npor antigas tropas de Drácula e alguns dos seus inimigos e fugitivos\n, as hordas de lobisomens foram usadas por políticos que tentaram defender a cidade de Lisboa\n, com a sua derrota os sobreviventes vieram para aqui junto com Bruxas \ne bruxos e criaram uma Terra pacífica para a sua vida. \nOutro renegados também se juntaram além de fugitivos de guerra e de Lisboa.\n Apesar do que se posso pensar Humanos são bem vindos e estão em segurança, exceto os políticos. \nÉ uma terra livre organizada por um conselho de Bruxos e Lobisomens mas também humanos.\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void historiaflbranca() {
    printf("\nAntiga floresta entre Espanha e Portugal, acabou invadida por Fadas que a tornaram a sua nova casa após o extermínio da Amazónia.\n Para protegerem a nova casa criaram borboletas mágicas que têm a capacidade de envenenar possíveis inimigos.\n Ninguém é bem vindo a esta floresta mas Humanos sobretudo mulher ou crianças desesperadas \nsão aceites e criadas pelas fadas com objetivos pouco claros.\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void historialisboa() {
    printf("\nAntiga capital política e corrupta de Portugal, centro de tudo o que havia de mau em Portugal\n e a principal causa do colapso de Portugal.\n Após ser quase arrasada por Marroquinos na reconquista dos mouros um grupo de \nVampiros enviados por Drácula tomaram conta do que restava da cidade e das pessoas e construiu uma quinta de alimentação com a ajuda de alguns antigos políticos\n e funcionários públicos, um deles Jerónimo de Sousa ainda “vivo” como zombie. O objetivo é manter a \ncidade como quinta para alimentar os vampiros e impedir a todo o custo a sua perda.\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void historiaporto() {
    printf("\nCidade milenar com milhares de anos com muitas influências de várias culturas, antiga centro de Portugal. ");
    printf("\nAtualmente após o colapso de Portugal esta cidade tornou-se uma cidade-estado mantendo a sua independência do Reino de Marrocos,");
    printf("\nem luta constante mas ao mesmo tempo mantém certas relações comerciais com Marrocos");
    printf("\nor causa do porto de Leixões e do FCP jogar na liga Marroquina e por norma dominar a mesma.");
    printf("\nVivem pessoas porreiras Conhecidos como os nortenhos ou tripeiros\n");
    int enter=1;
    while (enter != 0) {
        printf("Escreva 0/zero para continuar\n");
        enter = leituraint();

    }
    mapa();

}

void missaolisboa() {
    printf("\nEstás na entrada da cidade de Lisboa");
    printf("\nQueres falar com pessoas ou ir para o mapa?");
    int opcao = 0;
    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com pessoas");
        printf("\n2-- Se queres ir para o mapa\n");
        opcao = leituraint();
    }
    switch (opcao) {
        case 1:
            printf("\nEscolheu falar com pessoas!");
            opcao = 0;
            while (opcao == 0 || opcao < 0 || opcao > 2) {
                printf("\nNa entrada da cidade de Lisboa");
                printf("\n1-- Falar com o guarda na entrada?");
                printf("\n2-- Desistir e voltar ao mapa?..\n");
                opcao = leituraint();
                if (opcao == 1) {
                    printf("\nO guarda olha para ti e deixa sair os dentes, parece um vampiro");
                    printf("\nAté que tu decides falar ele, com suores frios");
                    if (personagem == 1) {

                        printf("\nAndré: Olá eu gostaria de entrar na cidade é possível?");
                        printf("\nVampiro: Porque queres entrar seu animal?");
                        opcao = 0;
                        while (opcao == 0 || opcao < 0 || opcao > 5) {
                            printf("\nEscolha uma resposta");
                            printf("\n1-- Quero entrar para trabalhar, sou forte e posso ser útil");
                            printf("\n2-- Eu sou Do Porto quero conhecer a floresta negra");
                            printf("\n3-- Eu não gosto desses dentes");
                            printf("\n4-- Problemas pessoais..");
                            printf("\n5-- Sai da frente chupa tolas\n");
                            opcao = leituraint();
                            if (opcao == 1 || opcao == 4) {
                                printf("\nEscolheste a resposta %d", opcao);
                                printf("\nO vampiro sorri e deixa-te entrar na cidade, ganhas ponto de nível, 10");
                                printf("\nEntraste na cidade de Lisboa e tens algumas missões ou seres com quem falar\n");
                                nivel = nivel + 10;
                                missaolisboa_2();

                            } else if (opcao == 5) {
                                printf("\nEscolheste a resposta %d", opcao);
                                int sorte = rand() % 100;

                                if (nivel <= 200 && sorte < 20) {
                                    printf("\nVampiro: O que me chamaste seu animal?");
                                    printf("\nNum momento para o outro o vampiro cresce e dois lobisomens aparecem\n");
                                    opcao = 0;
                                    while (opcao == 0 || opcao < 0 || opcao > 2) {
                                        printf("\nQueres insistir?");
                                        printf("\n1-- Sim");
                                        printf("\n2--Não\n");
                                        opcao = leituraint();
                                        if (opcao == 1) {
                                            sorte = rand() % 100;
                                            if (sorte < 90) {
                                                printf("\nÉs atacado pelos três e nem chance tens");
                                                saopedro();
                                            } else {
                                                printf("\nTás com sorte hoje e Zombie chamado Jerónimo deixa-te entrar, ganhaste 20 pontos no teu nível");
                                                nivel = nivel + 20;
                                                printf("\nEntraste na cidade de Lisboa e tens algumas missões ou pessoas com quem falar\n");
                                                missaolisboa_2();

                                            }
                                        } else {
                                            mapa();
                                        }
                                    }
                                }
                            } else if (opcao == 2) {
                                if (nivel <= 550) {
                                    printf("\nO vampiro salta-te e deixa-te sem sentidos");
                                    printf("\nPassas quase um ano como prisioneiro até servires de alimento\n");
                                    saopedro();
                                }
                                int sorte = rand() % 100;
                                if (nivel >= 550 && sorte < 75) {
                                    sorte = rand() % 3;
                                    if (sorte < 2) {
                                        printf("\nA tua força e o teu poder são enormes e arrancas a cabeça ao vampiro");
                                        printf("\nEntras pela cidade para a conheceres e sobres o nível em 50\n");
                                        nivel = nivel + 50;
                                        missaolisboa_2();


                                    } else {
                                        printf("\nFicas ferido no combate com o vampiro mas consegues fugir, menos 10 pontos de nível\n");
                                        nivel = nivel - 10;
                                        mapa();

                                    }

                                }
                                    }
                            else {
                                int sorte = rand() % 100;
                                if (nivel <= 550) {
                                    printf("\nO vampiro sorri e chama zombies tu olhas e voltas para trás\n");
                                    mapa();
                                } else if (nivel <= 550 && sorte < 10) {
                                    printf("\nTás com sorte o vampiro deixa-te entrar com um sorriso, ganhaste 5 pontos no teu nível");
                                    nivel = nivel + 5;
                                    printf("\nEntraste na cidade de Lisboa e tens algumas missões ou seres com quem falar\n");
                                    missaolisboa_2();
                                } else {
                                    printf("\nLutas com o vampiro mas nem o incomodas e foges ferido, menos 10 pontos de nível\n");
                                    nivel = nivel - 10;
                                    mapa();
                                }
                            }
                        }
                    } else if (personagem == 3) {
                        printf("\nHistória por implementar");
                        mapa();

                    }

                }
                break;
                case 2:
                    mapa();
                break;

            }


    }

}

void missaolisboa_2() {
    printf("\nEstás dentro da cidade de Lisboa");
    printf("\nTens 3 tipos de missões aqui");
    printf("\nO teu nível é igual a %d",nivel);
    printf("\nEscolhe bem o que queres, para não vires chorar depois\n");
    if (personagem == 1) {
        int opcao = 0 ;
        while (opcao == 0 || opcao < 0 || opcao > 4) {
            printf("\n1-- Se queres uma missão fácil");
            printf("\n2-- Se queres uma missão média");
            printf("\n3-- Se queres uma missão díficil");
            printf("\n4-- Se queres ir para o mapa\n");
            opcao = leituraint();
            if (opcao == 1){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2){
                    printf("\n1-- O Jerónimo");
                    printf("\n2-- O Costa\n");
                    opcao =leituraint();
                }
                if (opcao ==1){
                    printf("\nJerónimo: O que precisas camarada?");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nJerónimo: Então começa a colar cartazes do PCP");
                    printf("\nHomem: .....");
                    printf("\nJerónimo: Rápido, antes que os fachus apareçam!");
                    printf("\nTu colas cartazes até caíres para o chão e ganhas, 20 pontos de nível\n");
                    nivel = nivel + 20;
                    missaolisboa_2();

                } else{
                    printf("\nCosta: Olá parceiro, o que precisas?");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nCosta: Muito bem, preciso que me faças um favor");
                    printf("\nHomem: Que favor?");
                    printf("\nCosta: Preciso que me leves este pacote com farinha branca a uma casa");
                    printf("\nGanhas 10 pontos de nível, Levas o pacote à morada");
                    nivel = nivel +10;
                    missaolisboa_2();

                }
            }
            else if (opcao == 2){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma\n");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Um lobisomem");
                    printf("\n2-- Uma velha mulher\n");
                    opcao = leituraint();
                    if (opcao == 1){
                        printf("\nLobisomem: Olá homenzinho");
                        printf("\nHomem: Olá meu senhor");
                        printf("\nLobisomem: Queres trabalho?");
                        printf("\n Homem: Não eu quero é comer\n");
                        int sorte = rand()%100;
                        if (sorte < 30){
                            printf("\nLobisomem: Toma lá um bife cru então");
                            printf("\nTu comes, a carne tem um sabor estranho, ganhas 20 pontos de nível\n");
                            nivel = nivel +20;
                            missaolisboa_2();
                        }
                        else if (sorte < 75 && sorte >= 30){
                            printf("\nO Lobisomem olha para ti e dá-te um soco");
                            printf("\nGanhas 5 pontos de nível porque ficaste mais rijo\n");
                            nivel = nivel +5;
                            missaolisboa_2();
                        }
                        else{
                            printf("\nLobisomem: É o teu dia de sorte apanhei um porco ontem");
                            printf("\nComes o que queres e sobes 30 pontos de nível\n");
                            nivel = nivel + 30;
                            missaolisboa_2();
                        }
                    }
                    else if (opcao==2){
                        printf("\nVelha: Olá Homem Grande e feio");
                        printf("\nHomem: Olá minha senhora, precisa de ajuda?");
                        printf("\nVelha: Sim, estou com uma coisa nos dentes");
                        printf("\nHomem: Ok vou buscar um palito e ajudo-a\n");
                        int sorte = rand()%100;
                        if (sorte < 40){
                            printf("\nA velha tira a dentadura");
                            printf("\nEla ri-se e vai-se embora");
                            printf("\nO que conta é a intenção ganhas 10 pontos de nível\n");
                            nivel = nivel +10;
                            missaolisboa_2();
                        } else{
                            printf("\nFoste o maior, limpas aqueles dentes todos");
                            if (sorte <20){
                                printf("\nEla é uma bruxa, ganhas reputação e nível e sobes 40 pontos\n");
                                nivel =  nivel + 40;
                                missaolisboa_2();

                            }else if (sorte >= 20 && sorte <= 70){
                                printf("\nEla sorri e dá-te um beijo na cara e sobes 25 pontos de nível\n");
                                nivel = nivel + 25;
                                missaolisboa_2();

                            }else{
                                printf("\nEla olha para ti e dá-te dinheiro como recompensa");
                                printf("\nTu recusas porque és burro");
                                printf("\nSobes 5 pontos de nível\n");
                                nivel = nivel +5;
                                missaolisboa_2();


                            }
                        }
                    }
                }


            }

            else if (opcao==3){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Falar com o Vampiro Vamp");
                    printf("\n2-- Entrar no Estádio da Luz\n");
                    opcao = leituraint();
                    if (opcao==1){
                        if (nivel < 200){
                            printf("\nO chefe do vampiros Vamp tem mais que fazer\n");
                            missaoporto_2();
                        }else if (nivel >= 200){
                            int sorte = rand()%100;
                            if (sorte <50){
                                printf("\nO Vamp olha para ti com desprezo, à espera que fales");
                                printf("\nOlá meu lord, eu estou aqui para trabalhar e ser-lhe útil");
                                printf("\nVamp: A unica coisa que quero de ti é o teu sangue, apetece-me um copo\n");
                                opcao=0;
                                while (opcao == 0 || opcao < 0 || opcao > 2) {
                                    printf("\n1-- Aceitas");
                                    printf("\n2-- Recusas\n");
                                    opcao = leituraint();
                                    if (opcao == 2){
                                        printf("\nNinguém recusa nada ao Vamp, perdes 20 pontos\n");
                                        nivel = nivel -20;
                                        missaolisboa_2();

                                    }else{
                                        printf("\nTu deixas que um servo te abra uma veia para encher um copo de sangue");
                                        printf("\nMais 80 pontos de nível\n");
                                        nivel = nivel +80;
                                        missaolisboa_2();
                                    }
                                }
                            }
                        }
                    } else if (opcao==2){
                        printf("\nEntras no Estádio da Luz");
                        if (nivel <200){
                            printf("\nPéssima ideia, ficas a lavar a estatua de um antigo jogador de futebol");
                            printf("\nMenos 20 pontos de nível\n");
                            nivel = nivel -20;
                            missaolisboa_2();
                        } else{
                            int sorte = rand()%100;
                            if (sorte < 40){
                                printf("\nPéssima ideia, tens que dar de comer ao 'gado'");
                                printf("\nMenos 20 pontos de nível\n");
                                nivel = nivel -20;
                                missaoporto_2();
                            }
                            else{
                                printf("\nArranjas trabalho como alimentador, és bem pago porque nem todos humanos trabalham contra eles");
                                printf("\nGanhas 70 pontos de nível\n");
                                nivel = nivel +70;
                                missaoporto_2();
                            }
                        }
                    }
                }
            }else
            {mapa();
            }

        }

    }
}

void castelodrac(){
    printf("\nEstás na entrada do Castelo do Drácula");
    printf("\nQueres falar com pessoas ou ir para o mapa?");
    int opcao = 0;

    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com pessoas");
        printf("\n2-- Se queres ir para o mapa\n");
        opcao = leituraint();
    }
    switch (opcao) {
        case 1:
            printf("\nEscolheu falar com pessoas!");
            opcao = 0;
            while (opcao == 0 || opcao < 0 || opcao > 2) {
                printf("\nNa entrada do Castelo de Drácula");
                printf("\n1-- Falar com o guarda na entrada?");
                printf("\n2-- Voltar ao mapa\n");

                opcao = leituraint();
                if (opcao == 1) {
                    printf("\nO guarda meio peludo olha para ti, lobisomem ao que parece");
                    printf("\nAté que tu decides falar com ele");
                    if (personagem == 1) {

                        printf("\nAndré: Olá eu gostaria de entrar no castelo é possível?");
                        printf("\nLobisomem: Porque queres entrar Homem rapado?");
                        opcao = 0;
                        while (opcao == 0 || opcao < 0 || opcao > 5) {
                            printf("\nEscolha uma resposta");
                            printf("\n1-- Quero entrar para trabalhar");
                            printf("\n2-- Eu sou Do Porto quero conhecer o castelo");
                            printf("\n3-- Quero conhecer o Conde Vlad");
                            printf("\n4-- Problemas pessoais.. ");
                            printf("\n5-- Eu entro onde quero\n");
                            opcao = leituraint();
                            if (opcao == 1 || opcao == 2) {
                                printf("\nEscolheste a resposta %d", opcao);
                                printf("\nO lobisomem olha para ti e permite a entrada,ganhas nivel 10");
                                printf("\nEntraste no Castelo de Vlad o impalador e tens algumas missões ou seres com quem falar");
                                nivel = nivel + 10;
                                castelodrac_2();

                            } else if (opcao == 3) {
                                printf("\nEscolheste a resposta %d", opcao);
                                int sorte = rand() % 100;

                                if (nivel <= 200 && sorte < 20) {
                                    printf("\nLobisomem: O conde tem mais que fazer do que te conhecer");

                                    opcao = 0;
                                    while (opcao == 0 || opcao < 0 || opcao > 2) {
                                        printf("Queres insistir?");
                                        printf("1-- Sim");
                                        printf("2--Não");
                                        opcao = leituraint();
                                        if (opcao == 1) {
                                            sorte = rand() % 100;
                                            if (sorte < 90) {
                                                printf("\nEstás sem sorte o Monstro puxa da espada e corta-te em dois");
                                                saopedro();
                                            } else {
                                                printf("\nTás com sorte e o guarda permite a tua entrada, ganhaste 20 pontos no teu nível");
                                                nivel = nivel + 20;
                                                printf("\nEntraste no Castelo do Drácula e tens algumas missões ou pessoas com quem falar");
                                                castelodrac_2();

                                            }
                                        } else {
                                            printf("\nFugiste do Castelo\n");
                                            mapa();
                                        }
                                    }
                                }
                            } else if (opcao == 4) {
                                if (nivel <= 550) {
                                    printf("\nO monstro olha para ti, puxa de espada e corta-te a cabeça");
                                    saopedro();
                                }
                                int sorte = rand() % 100;
                                if (nivel >= 550 && sorte < 75) {
                                    sorte = rand() % 3;
                                    if (sorte < 2) {
                                        printf("\nO monstro tenta impedir-te mas tu defendes a posição e atiras a coisa ao fosso do castelo");
                                        printf("\nEntras no Castelo para o conheceres e sobres o nível em 50");
                                        nivel = nivel + 50;
                                        castelodrac_2();


                                    } else {
                                        printf("\nFicas ferido pelo confronto com o Lobisomem e foges, menos 10 pontos de nível");
                                        nivel = nivel - 10;
                                        mapa();

                                    }

                                }
                            }
                            else {
                                int sorte = rand() % 100;
                                if (nivel <= 550) {
                                    printf("\nO Lobisomem ignora-te e tu desistes e voltas para trás");
                                    mapa();
                                } else if (nivel <= 550 && sorte < 10) {
                                    printf("\nTás com sorte hoje e o guarda peludo deixa-te entrar, ganhaste 5 pontos no teu nível");
                                    nivel = nivel + 5;
                                    printf("\nEntraste no Castelo do Drácula e tens algumas missões ou seres com quem falar");
                                    castelodrac_2();
                                } else {
                                    printf("\nQuase morres a tentar passar pelo peludinho mas foges, menos 10 pontos de nível");
                                    nivel = nivel - 10;
                                    mapa();
                                }
                            }
                        }
                    } else if (personagem == 3) {
                        printf("\nHistória por implementar");
                        mapa();

                    }

                } else{
                    mapa();
                }
                break;
                case 2:
                    mapa();
                break;

            }


    }
}

void castelodrac_2() {
    printf("\nEstás dentro do Castelo de Vlad 'Drácula'");
    printf("\nTens 3 tipos de missões aqui");
    printf("\nO teu nível é igual a %d",nivel);
    printf("\nEscolhe bem o que queres, para não vires chorar depois");
    if (personagem == 1) {
        int opcao = 0 ;
        while (opcao == 0 || opcao < 0 || opcao > 4) {
            printf("\n1-- Se queres uma missão fácil");
            printf("\n2-- Se queres uma missão média");
            printf("\n3-- Se queres uma missão díficil");
            printf("\n4-- Se queres ir para o mapa\n");
            opcao = leituraint();
            if (opcao == 1){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                printf("\n1-- A Carmilla");
                printf("\n2-- O Nosfarato");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2){
                    printf("\n1-- A Carmilla");
                    printf("\n2-- O Nosfarato\n");
                    opcao =leituraint();
                }
                if (opcao ==1){
                    printf("\nCarmilla: Ó seboso o que queres?");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nCarmilla: Então começa a limpar a ponte levadiça do castelo");
                    printf("\nHomem: .....");
                    printf("\nRápido, antes que eu te faça meu escravo!");
                    printf("\nTu limpas tudo até te poderes ver ao espelho no chão, 20 pontos de nível\n");
                    nivel = nivel + 20;
                    castelodrac_2();

                } else{
                    printf("\nNosfarato: Olá Home, o que queres de Nosfarato");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nNosfarato: Hahahahahaha");
                    printf("\nHomem: ....?");
                    printf("\nNosfarato: Foge, foge....");
                    printf("\nGanhas 5 pontos de nível, não sei o que esperavas do Vampriro mas pronto\n");
                    nivel = nivel +5;
                    castelodrac_2();

                }
            }
            else if (opcao == 2){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma\n");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Um monstro no corredor");
                    printf("\n2-- Uma mulher linda\n");
                    opcao = leituraint();
                    if (opcao == 1){
                        printf("\nMonstro: Arghh");
                        printf("\nHomem: Olá meu senhor");
                        printf("\nMonstro: Argh?");
                        printf("\n Homem: Não eu quero é comer\n");
                        int sorte = rand()%100;
                        if (sorte < 30){
                            printf("\nMonstro: Ele tira da bolso uma sandes e tu comes");
                            printf("\nComes aquilo a fome é negra e não é fácil ganhas 20 pontos de nível\n");
                            nivel = nivel +20;
                            castelodrac_2();
                        }
                        else if (sorte < 75 && sorte >= 30){
                            printf("\nO monstro diz 'Arrhg'");
                            printf("\nGanhas 5 pontos de nível porque ficaste sem entender nada\n");
                            nivel = nivel +5;
                            castelodrac_2();
                        }
                        else{
                            printf("\nMonstro: Argh!! ");
                            printf("\nComes uma sandes e uma coxa que cai no chão e sobes 30 pontos de nível\n");
                            nivel = nivel + 30;
                            missaoporto_2();
                        }
                    }
                    else if (opcao==2){
                        printf("\nMulher: Olá Homem Grande e sexy");
                        printf("\nHomem: Olá princesa, precisas de ajuda?");
                        printf("\nMulher: Sim, quero-te comer");
                        printf("\nHomem: O quê? Não entendi\n");
                        int sorte = rand()%100;
                        if (sorte < 40){
                            printf("\nEla salta para cima de ti suga-te o sangue todo, morres...");
                            printf("\nEla ri-se e vai-se embora");
                            printf("\nPerdes 10 de nivel e boa sorte com o São Pedro\n");
                            nivel = nivel -10;
                            saopedro();
                        } else{
                            printf("\nFoste o maior, foges do ataque da Vampira");
                            if (sorte <20){
                                printf("\nEla é filha de  Drácula, ganhas reputação e nível e sobes 40 pontos\n");
                                nivel =  nivel + 40;
                                castelodrac_2();

                            }else if (sorte >= 20 && sorte <= 70){
                                printf("\nEla sorri e dá-te um beijo na cara e sobes 25 pontos de nível, alivio...\n");
                                nivel = nivel + 25;
                                castelodrac_2();

                            }else{
                                printf("\nEla olha para ti salta-te e suga-te sangue");
                                printf("\nTu defendes-te e ela ri-se e deixa-te vivo");
                                printf("\nSobes 5 pontos de nível\n");
                                nivel = nivel +5;
                                castelodrac_2();


                            }
                        }
                    }
                }


            }

            else if (opcao==3){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma\n");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Falar com o Drácula");
                    printf("\n2-- Entrar nas Catacumbas\n");
                    opcao = leituraint();
                    if (opcao==1){
                        if (nivel < 200){
                            printf("\nO Drácula nem é avisado da tua chegada\n");
                            castelodrac_2();
                        }else if (nivel >= 200){
                            int sorte = rand()%100;
                            if (sorte <50){
                                printf("\nO Drácula C olha para ti com desprezo, à espera que fales");
                                printf("\nOlá iminência, eu estou aqui para trabalhar e ser-lhe útil");
                                printf("\nDrácula: Seu pedaço de carne, vai-me buscar um humano estou com fome\n");
                                opcao=0;
                                while (opcao == 0 || opcao < 0 || opcao > 2) {
                                    printf("\n1-- Aceitas");
                                    printf("\n2-- Recusas\n");
                                    opcao = leituraint();
                                    if (opcao == 2){
                                        printf("\nNinguém recusa nada ao Drácula ele salta e bebe o teu sangue já foste, perdes 20 pontos\n");
                                        nivel = nivel -20;
                                        saopedro();

                                    }else{
                                        printf("\nTu vais buscar um Homem jovem bonito e saudavel a uma cela");
                                        printf("\nMais 80 pontos de nível\n");
                                        nivel = nivel +80;
                                        castelodrac_2();
                                    }
                                }
                            }
                        }
                    } else if (opcao==2){
                        printf("\nEntras nas catacumbas");
                        if (nivel <200){
                            printf("\nPéssima ideia, levas uma sova");
                            printf("\nMenos 20 pontos de nível\n");
                            nivel = nivel -20;
                            castelodrac_2();
                        } else{
                            int sorte = rand()%100;
                            if (sorte < 40){
                                printf("\nPéssima ideia, matam-te e comem-te");
                                printf("\nMenos 20 pontos de nível\n");
                                nivel = nivel -20;
                                saopedro();
                            }
                            else{
                                printf("\nEncontras ouro num saco perdido no chão");
                                printf("\nGanhas 70 pontos de nível\n");
                                nivel = nivel +70;
                                castelodrac_2();
                            }
                        }
                    }
                }
            }else
            {
                mapa();
            }

        }

    }
}

void saopedro() {
    if (personagem==1 || personagem == 2){
    printf("\nEstás na entrada do Ceú com a frente do São Pedro");
    int opcao = 0;

    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com o São Pedro");
        printf("\n2-- Se queres ignorar o São Pedro\n");
        opcao = leituraint();
    }
    switch (opcao) {
        case 1:
            printf("\nEscolheu falar com o São Pedro!");
            opcao = 0;
            while (opcao == 0 || opcao < 0 || opcao > 3) {
                printf("\nO São Pedro olha para ti a espera que fales");
                printf("\n1-- Falar com o São Pedro de forma humilde?");
                printf("\n2-- Ser arrogante");
                printf("\n3-- Desistir\n)");
                opcao = leituraint();
                if (opcao == 1) {
                    printf("\nOlá São Pedro");
                    printf("\nEstou aqui a tua frente Humildemente para falar contigo\n");
                    if (personagem == 1) {

                        printf("\nSão Pedro: Olá meu filho, foste uma boa pessoa na tua vida?");

                        opcao = 0;
                        while (opcao == 0 || opcao < 0 || opcao > 5) {
                            printf("\nEscolha uma resposta");
                            printf("\n1-- Nem por isso, lutei e matei");
                            printf("\n2-- Sim fui, sempre ajudei os outros");
                            printf("\n3-- Não sei...");
                            printf("\n4-- Eu sou um Santo");
                            printf("\n5-- Eu sou um demônio e desprezo religião\n");
                            opcao = leituraint();
                            if (opcao == 1 || opcao == 2) {
                                printf("\nEscolheste a resposta %d", opcao);
                                printf("\nO São Pedro fica calado uns minutos e diz");
                                printf("\nVais para o inferno que é onde mereces estar\n");

                                missaoinferno();

                            } else if (opcao == 3) {
                                printf("\nEscolheste a resposta %d", opcao);
                                int sorte = rand() % 100;

                                if (nivel <= 200 && sorte < 20) {
                                    printf("\nSão Pedro: Não sabes? É essa a tua resposta?\n");

                                    opcao = 0;
                                    while (opcao == 0 || opcao < 0 || opcao > 2) {
                                        printf("\nQueres insistir?");
                                        printf("\n1-- Sim");
                                        printf("\n2--Não\n");
                                        opcao = leituraint();
                                        if (opcao == 1) {
                                            sorte = rand() % 100;
                                            if (sorte < 90) {
                                                printf("\nSão Pedro: Ok, ninguém sabe mesmo, espero que te melhores no paraíso\n");
                                                paraiso();
                                            } else {
                                                printf("\nSão Pedro: Bem já que não sabes vais voltar a Terra a ver se aprendes\n");

                                                printf("\nDás por ti nu, caído no chão de novo vivo\n");
                                                mapa();

                                            }
                                        } else {
                                            printf("\nSão Pedro: Agora ficaste calado?");
                                            printf("\nJá que ficaste calado vais para o inferno lá vais aprender a falar\n");
                                            missaoinferno();
                                        }
                                    }
                                }
                            } else if (opcao == 4) {
                                if (nivel <= 550) {
                                    printf("\nSão Pedro: Tu não és ninguém boa sorte no Inferno\n");
                                    missaoinferno();
                                }
                                int sorte = rand() % 100;
                                if (nivel >= 550 && sorte > 95) {
                                    sorte = rand() % 3;
                                    if (sorte == 2) {
                                        printf("\nO São Pedro sorri e deixa-te entrar no paraíso");
                                        printf("\nEntras no paraíso\n");
                                        paraiso();

                                    } else {
                                        printf("\nO São  Pedro não diz nada e és sugado por um buraco infernal\n");
                                        missaoinferno();

                                    }

                                }
                            }else if (opcao==5){
                                int sorte = rand() % 100;
                                if (nivel <= 550) {
                                    printf("\nSão Pedro: Um demônio? Religião? Isso não me interessa nada boa sorte no paraíso\n");
                                    paraiso();
                                } else if (nivel <= 550 && sorte < 10) {
                                    printf("\nSão Pedro: volta a terra e aprende alguma coisa seu otário");

                                    printf("\nDás por ti nu, caído no chão de novo vivo\n");
                                    mapa();
                                } else {
                                    printf("\nSão Pedro: Venha o próximo\n");

                                    missaoinferno();
                                }
                            }
                        }
                    } else if (personagem == 2) {

                    }

                } else if (opcao == 2) {
                    missaoinferno();

                } else {
                    missaoinferno();
                }
                break;
                case 2:
                    missaoinferno();
                break;

            }


    }

}else if (personagem==3){
        printf("\nBem estás a olhar para o São Pedro, como semi-deus andas por onde queres\n");
        printf("\nEle ri-se para ti e tu voltas ao mapa\n");
        mapa();
    }
}

void paraiso() {

    printf("\nEstás no Paraíso");
    if (personagem ==1 || personagem ==2){
    //printf("\nQueres falar com pessoas ou ir para o mapa?");
    int opcao = 0;
    if (personagem == 3){
    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com seres");
        printf("\n2-- Se queres ir para o mapa\n");
        opcao = leituraint();}
        if (opcao == 2){
            mapa();
        }
    }
    switch (opcao) {
        case 1:
            printf("\nEstás no Paraíso, todo o tipo de seres existem por aqui!");
            opcao = 0;
            if (personagem == 1) {

                printf("\nAndré: Olá eu gostaria falar com Deus é possível?");
                printf("\nAnjo: argh...?");
                opcao = 0;
                while (opcao == 0 || opcao < 0 || opcao > 5) {
                    printf("\nEscolha uma resposta");
                    printf("\n1-- Quero agradecer-lhe");
                    printf("\n2-- Eu sou Do Porto quero conhecer o paraíso e o seu criador");
                    printf("\n3-- Estou doente e preciso de ajuda");
                    printf("\n4-- Tu és o quê?.. ");
                    printf("\n5-- Sai da frente...\n");
                    opcao = leituraint();
                    if (opcao == 1 || opcao == 2 || opcao==3) {
                        printf("\nEscolheste a resposta %d", opcao);
                        printf("\nO anjo fica calado");
                        printf("\nE desaparece");
                        nivel = nivel + 10;
                        paraiso();

                    } else if (opcao == 4) {
                        printf("\nEscolheste a resposta %d", opcao);
                        int sorte = rand() % 100;

                        if (nivel <= 200 && sorte < 20) {
                            printf("\nO anjo fica em silêncio\n");

                            opcao = 0;
                            while (opcao == 0 || opcao < 0 || opcao > 2) {
                                printf("\nQueres insistir?");
                                printf("\n1-- Sim");
                                printf("\n2--Não\n");
                                opcao = leituraint();
                                if (opcao == 1) {
                                    sorte = rand() % 100;
                                    if (sorte < 90) {
                                        printf("\nO anjo não gosta de curiosos\n");
                                        missaoinferno();
                                    } else {
                                        printf("\nTás com sorte é Grabriel e conta-te uma história, ganhaste 20 pontos no teu nível\n");
                                        nivel = nivel + 20;

                                        paraiso();

                                    }
                                } else {
                                    printf("\nO anjo olha para ti para não te aturar manda-te para a Terra\n");
                                    mapa();
                                }
                            }
                        }
                    } else if (opcao == 5) {
                        if (nivel <= 550) {
                            printf("\nÉs agarrado pelo anjo");
                            printf("\nQuando dás conta estás num local quente\n");
                            missaoinferno();
                        }
                        int sorte = rand() % 100;
                        if (nivel >= 550 && sorte < 95) {
                            sorte = rand() % 3;
                            if (sorte < 2) {
                                printf("\nConseguiste falar com o anjo, ele sorri e...");
                                printf("\nJá foste\n");

                                missaoinferno();


                            }

                        }
                    }
                }
            } else if (personagem == 2) {

            }


            break;
                case 2:
                    paraiso();
                break;

            }


    } else if (personagem ==3){
        printf("\nBem estás no paraíso, como semi-deus andas por onde queres\n");
        printf("\nGabriel ri-se para ti e tu voltas ao mapa\n");
        mapa();
    }
}


/*void casteloreia(){
    printf("\nEstás na entrada do Castelo do Rei Artur");
    printf("\nQueres falar com pessoas ou ir para o mapa?");
    int opcao = 0;
}*/

void missaoflorestanegra() {
    printf("\nEstás na entrada da Floresta Negra");
    printf("\nQueres falar com pessoas ou ir para o mapa?");
    int opcao = 0;
    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com pessoas");
        printf("\n2-- Se queres ir para o mapa\n");
        opcao = leituraint();
    }
    switch (opcao) {
        case 1:
            printf("\nEscolheu falar com pessoas!");
            opcao = 0;
            while (opcao == 0 || opcao < 0 || opcao > 2) {
                printf("\nNa entrada da floresta negra");
                printf("\n1-- Falar com ser ser na entrada?");
                printf("\n2-- Desistir e voltar ao mapa?..\n");
                opcao = leituraint();
                if (opcao == 1) {
                    printf("\nO ser sai da sombras e parece um zombie");
                    printf("\nAté que tu decides falar com ele, meio aterrorizado...");
                    if (personagem == 1) {

                        printf("\nAndré: Olá eu gostaria de entrar na floresta é possível?");
                        printf("\nZombie: argh...?");
                        opcao = 0;
                        while (opcao == 0 || opcao < 0 || opcao > 5) {
                            printf("\nEscolha uma resposta");
                            printf("\n1-- Quero entrar para descansar");
                            printf("\n2-- Eu sou Do Porto quero conhecer a floresta negra e encontrar os bruxos");
                            printf("\n3-- Estou doente e preciso de ajuda");
                            printf("\n4-- Tu és o quê?.. ");
                            printf("\n5-- Sai da frente...\n");
                            opcao = leituraint();
                            if (opcao == 1 || opcao == 2) {
                                printf("\nEscolheste a resposta %d", opcao);
                                printf("\nO zombie fica calado e tu entras pela floresta, ganhas ponto de nível, 10");
                                printf("\nEntraste na Floresta Negra e tens algumas missões ou seres com quem falar\n");
                                nivel = nivel + 10;
                                missaoflorestanegra_2();

                            } else if (opcao == 3) {
                                printf("\nEscolheste a resposta %d", opcao);
                                int sorte = rand() % 100;

                                if (nivel <= 200 && sorte < 20) {
                                    printf("\nO Zombie deixa de estar sozinho e uma horda aparece\n");

                                    opcao = 0;
                                    while (opcao == 0 || opcao < 0 || opcao > 2) {
                                        printf("\nQueres insistir?");
                                        printf("\n1-- Sim");
                                        printf("\n2--Não\n");
                                        opcao = leituraint();
                                        if (opcao == 1) {
                                            sorte = rand() % 100;
                                            if (sorte < 90) {
                                                printf("\nOs Zombies Atacam e és comido\n");
                                                saopedro();
                                            } else {
                                                printf("\nTás com sorte e consegues correr pela floresta dentro, ganhaste 20 pontos no teu nível");
                                                nivel = nivel + 20;
                                                printf("\nEntraste na Floresta Negra e tens algumas missões ou pessoas com quem falar\n");
                                                missaoflorestanegra_2();

                                            }
                                        } else {
                                            mapa();
                                        }
                                    }
                                }
                            } else if (opcao == 5) {
                                if (nivel <= 550) {
                                    printf("\nÉs agarrado por umas mãos que saem da Terra");
                                    printf("\nQuando dás conta tás com as tripas de fora a seres comido por zombies\n");
                                    saopedro();
                                }
                                int sorte = rand() % 100;
                                if (nivel >= 550 && sorte < 95) {
                                    sorte = rand() % 3;
                                    if (sorte < 2) {
                                        printf("\nConseguiste matar o Zombie");
                                        printf("\nEntras pela floresta para a conheceres e sobres o nível em 50\n");
                                        nivel = nivel + 50;
                                        missaoflorestanegra_2();


                                    } else {
                                        printf("\nQuando dás conta tás rodeado por zombies mas lá foges como cobarde que és, menos 10 pontos de nível\n");
                                        nivel = nivel - 10;
                                        mapa();

                                    }

                                }
                            }else {
                                int sorte = rand() % 100;
                                if (nivel <= 550) {
                                    printf("\nUma horda de zombies aparece e tu recuas\n");
                                    mapa();
                                } else if (nivel <= 550 && sorte < 10) {
                                    printf("\nTás com sorte o zombie vira costas e entras, ganhaste 5 pontos no teu nível");
                                    nivel = nivel + 5;
                                    printf("\nEntraste na Floresta Negra e tens algumas missões ou seres com quem falar\n");
                                    missaoflorestanegra_2();
                                } else {
                                    printf("\nO zombie começa a correr na tua direcção mas tu corres mais, menos 10 pontos de nível\n");
                                    nivel = nivel - 10;
                                    mapa();
                                }
                            }
                        }
                    } else if (personagem == 3) {
                        printf("\nHistória por implementar");
                        mapa();

                    }

                }
                break;
                case 2:
                    mapa();
                break;

            }


    }

}

void missaoflorestanegra_2() {
    printf("\nEstás dentro da floresta negra");
    printf("\nTens 3 tipos de missões aqui");
    printf("\nO teu nível é igual a %d",nivel);
    printf("\nEscolhe bem o que queres, para não vires chorar depois\n");
    if (personagem == 1) {
        int opcao = 0 ;
        while (opcao == 0 || opcao < 0 || opcao > 4) {
            printf("\n1-- Se queres uma missão fácil");
            printf("\n2-- Se queres uma missão média");
            printf("\n3-- Se queres uma missão díficil");
            printf("\n4-- Se queres ir para o mapa\n");
            opcao = leituraint();
            if (opcao == 1){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");

                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2){
                    printf("\n1-- A Vanessa");
                    printf("\n2-- O Alfredo\n");
                    opcao =leituraint();
                }
                if (opcao ==1){
                    printf("\nVanessa: Oia o que queres?");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nVanessa: Trabalho na floresta?");
                    printf("\nHomem: .....");
                    printf("\nNão és muito inteligente né? Vais buscar lenha para a fogueira!");
                    printf("\nTu apanhas lenha para o fogo e pronto, 20 pontos de nível\n");
                    nivel = nivel + 20;
                    missaoflorestanegra_2();

                } else{
                    printf("\nAlfredo: Olá Homem, Olá Olá Olá");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nAlfredo: HAHAHA Então senta-te");
                    printf("\nHomem: Não entendi!?");
                    printf("\nAlfredo: HAHAHA");
                    printf("\nGanhas 5 pontos de nível, o Alfredo era doido\n");
                    nivel = nivel +5;
                    missaoflorestanegra_2();

                }
            }
            else if (opcao == 2){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Zombie");
                    printf("\n2-- Lobisomem\n");
                    opcao = leituraint();
                    if (opcao == 1){
                        printf("\nZombie: Oarhhahs!");
                        printf("\nHomem: Olá meu senhor");
                        printf("\nZombie: ???ahrhhrh");
                        printf("\n Homem: AH?\n");
                        int sorte = rand()%100;
                        if (sorte < 30){
                            printf("\nZombie: arhhrhghhshsh!");
                            printf("\nNão é fácil falar com zombies, nada fácil ganhas 20 pontos de nível\n");
                            nivel = nivel +20;
                            missaoflorestanegra_2();
                        }
                        else if (sorte < 75 && sorte >= 30){
                            printf("\nO Zombie salta-te em cima e cai no chão sem te ferir");
                            printf("\nGanhas 5 pontos de nível porque ficaste mais rijo\n");
                            nivel = nivel +5;
                            missaoflorestanegra_2();
                        }
                        else{
                            printf("\nMatas o zombie e encontras ouro nas roupas dele");
                            printf("\nE sobes 30 pontos de nível\n");
                            nivel = nivel + 30;
                            missaoflorestanegra_2();
                        }
                    }
                    else{
                        printf("\nobisomem: Olá vitela");
                        printf("\nHomem: Olá senhor peludo");
                        printf("\nLobisomem: Anda comigo vitela");
                        printf("\nHomem: Ok....\n");
                        int sorte = rand()%100;
                        if (sorte < 40){
                            printf("\nCaíste na armadilha e és comido");
                            printf("\nO Lobi fica cheio");
                            printf("\nO que conta é a intenção ganhas 10 pontos de nível, mas morres\n");
                            nivel = nivel +10;
                            saopedro();
                        } else{
                            printf("\nFoste o maior, ajudas o lobisomem a abrir uma cave");
                            if (sorte <20){
                                printf("\nEle divide o ouro contigo, ganhas reputação e nível e sobes 40 pontos\n");
                                nivel =  nivel + 40;
                                missaoflorestanegra_2();

                            }else if (sorte >= 20 && sorte <= 70){
                                printf("\nEle sorri e dá-te um beijo na cara e fazem amor , sobes 25 pontos de nível\n");
                                nivel = nivel + 25;
                                missaoflorestanegra_2();

                            }else{
                                printf("\nEle olha para ti e dá-te dinheiro como recompensa");
                                printf("\nTu recusas porque és burro");
                                printf("\nSobes 5 pontos de nível\n");
                                nivel = nivel +5;
                                missaoflorestanegra_2();


                            }
                        }
                    }
                }


            }

            else if(opcao==3){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Falar com a Bruxa-mor");
                    printf("\n2-- Ir até ao centro da floresta\n");
                    opcao = leituraint();
                    if (opcao==1){
                        if (nivel < 200){
                            printf("\nA Bruxa-mor não te quer receber\n");
                            missaoflorestanegra_2();
                        }else if (nivel >= 200){
                            int sorte = rand()%100;
                            if (sorte <50){
                                printf("\nA bruxa olha pra ti, à espera que fales");
                                printf("\nOlá iminência, eu estou aqui para trabalhar e ser-lhe útil");
                                printf("\nLili: Meu filho então vai-me buscar estas ervas na floresta\n");
                                opcao=0;
                                while (opcao == 0 || opcao < 0 || opcao > 2) {
                                    printf("\n1-- Aceitas");
                                    printf("\n2-- Recusas\n");
                                    opcao = leituraint();
                                    if (opcao == 2){
                                        printf("\nNinguém recusa nada à bruxa, ela olha-te nos olhos e viras pedra, perdes 20 pontos\n");
                                        nivel = nivel -20;
                                        saopedro();

                                    }else{
                                        printf("\nTu vais buscar tudinho, no fim és bem pago e ficas mais forte");
                                        printf("\nMais 80 pontos de nível\n");
                                        nivel = nivel +80;
                                        missaoflorestanegra_2();
                                    }
                                }
                            }
                        }
                    } else{
                        printf("\nEntras numa clareira no centro da floresta");
                        if (nivel <200){
                            printf("\nPéssima ideia, está lá um culto... nem te digo o que aconteceu");
                            printf("\nMenos 20 pontos de nível\n");
                            nivel = nivel -20;
                            saopedro();
                        } else{
                            int sorte = rand()%100;
                            if (sorte < 40){
                                printf("\nPéssima ideia, tropeçaste e caiste num buraco");
                                printf("\nMenos 20 pontos de nível\n");
                                nivel = nivel -20;
                                missaoflorestanegra_2();
                            }
                            else{
                                printf("\nAlguém deixou um objeto mágico de ouro no chão, tu pegas nele\nAchado não é roubado");
                                printf("\nGanhas 70 pontos de nível\n");
                                nivel = nivel +70;
                                missaoflorestanegra_2();
                            }
                        }
                    }
                }
            }

        }

    }
}

void missaoinferno() {
    printf("\nEstás no Inferno");
    if (personagem==1|| personagem ==2){
    int opcao = 0;
    printf("\nUma personagem bonita chega perto de ti e diz");
    printf("\nOlá lindo eu sou Lúcifer bem-vindo");

    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com Lúcifer");
        printf("\n2-- Se queres ignorar Lúcifer\n");
        opcao = leituraint();
    }
    switch (opcao) {
        case 1:
            printf("\nEscolheu falar com Lúcifer, sem saber quantas vezes o ignorou!");
            opcao = 0;
            while (opcao == 0 || opcao < 0 || opcao > 2) {
                printf("\nLúcifer a sorrir olha para ti a espera que fales");
                printf("\n1-- Falar com Lúcifer de forma humilde?");
                printf("\n2-- Ser arrogante\n");

                opcao = leituraint();
                if (opcao == 1) {
                    printf("\nOlá Lúcifer");
                    printf("\nEstou aqui a tua frente Humildemente para falar contigo");
                    if (personagem == 1) {

                        printf("\nLúcifer: Olá André, já vi que foste despachado para aqui pelo barbas");
                        printf("\nLúcifer: O que achas que te espera aqui?");

                        opcao = 0;
                        while (opcao == 0 || opcao < 0 || opcao > 5) {
                            printf("\nEscolha uma resposta");
                            printf("\n1-- Não sei...");
                            printf("\n2-- Ser torturado...");
                            printf("\n3-- Redempção...");
                            printf("\n4-- Não sei porque aqui estou.. Não sei o que esperar");
                            printf("\n5-- Eu espero ser teu amigo,odeio Deus e religião\n");
                            opcao = leituraint();
                            if (opcao == 1 || opcao == 2) {
                                printf("\nEscolheste a resposta %d", opcao);
                                printf("\nLúcifer sorri e fica calado uns minutos e diz");
                                printf("\nVais ser torturado, massacrado, morrer e renascer o resto dos tempos\n");


                                missaoinferno();

                            } else if (opcao == 3) {
                                printf("\nEscolheste a resposta %d", opcao);
                                int sorte = rand() % 100;

                                if (nivel <= 200 && sorte < 20) {
                                    printf("\nLúcifer: hahahahaha isso é uma ilusão humana");
                                    printf("\nLúcifer: Até já hahahaha\n");
                                    missaoinferno();
                                }

                            }
                            else if (opcao == 4) {
                                printf("\nLúcifer:adoro ignorantes");
                                printf("\n100 anos a ser torturado e voltas a Terra");
                                printf("\nAfinal acordas noutro local\n");
                                missaoinferno();


                            }
                            else if (opcao==5){
                                printf("\nLúcifer: E EU ODEIO QUEM FALA MAL DO MEU PAI");
                                printf("\nÉs torturado por séculos e perdes nível, 100\n");
                                nivel = nivel -100;
                                missaoinferno();
                            }
                        }
                    }
                } else if (personagem == 2) {

                } else if (opcao == 2) {
                    printf("\nLúcifer:adoro arrogantes");
                    printf("\n100 anos a ser torturado e voltas a Terra");
                    printf("\nAcordas na Terra vestido de mulher\n");
                    mapa();
                }
                break;
                case 2:
                    missaoinferno();
                break;

            }


    }
} else if (personagem==3){
        printf("\nBem estás no inferno, como semi-deus andas por onde queres\n");
        printf("\nLúcifer ri-se para ti e tu voltas ao mapa\n");
        mapa();
    }}

void missaoflorestabranca() {
    printf("\nEstás na entrada da Floresta Branca");
    printf("\nQueres falar com pessoas ou ir para o mapa?");
    int opcao = 0;
    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com pessoas");
        printf("\n2-- Se queres ir para o mapa\n");
        opcao = leituraint();
    }
    switch (opcao) {
        case 1:
            printf("\nEscolheu falar com pessoas!");
            opcao = 0;
            while (opcao == 0 || opcao < 0 || opcao > 2) {
                printf("\nNa entrada da floresta");
                printf("\n1-- Falar com a borboleta na entrada?");
                printf("\n2-- Desistir e voltar ao mapa?..\n");
                opcao = leituraint();
                if (opcao == 1) {
                    printf("\nA borboleta chama as companheiras e já são centenas");
                    printf("\nAté que tu decides falar com elas ou para elas");
                    if (personagem == 1) {

                        printf("\nAndré: Olá eu gostaria de entrar na floresta é possível?");
                        printf("\nSom da Natureza: Porque queres entrar Homem feio?");
                        opcao = 0;
                        while (opcao == 0 || opcao < 0 || opcao > 5) {
                            printf("\nEscolha uma resposta");
                            printf("\n1-- Quero entrar para descansar e trabalhar, sou honesto");
                            printf("\n2-- Eu sou Do Porto quero conhecer a floresta branca e encontrar fadas");
                            printf("\n3-- Estou doente e preciso de ajuda");
                            printf("\n4-- Problemas pessoais.. ");
                            printf("\n5-- Eu entro onde quero\n");
                            opcao = leituraint();
                            if (opcao == 1 || opcao == 3) {
                                printf("\nEscolheste a resposta %d", opcao);
                                printf("\nAs borboletas abrem um corredor mágico pela floresta, ganhas ponto de nível, 10");
                                printf("\nEntraste na Floresta Branca e tens algumas missões ou seres com quem falar\n");
                                nivel = nivel + 10;
                                missaoflorestabranca_2();

                            } else if (opcao == 4) {
                                printf("\nEscolheste a resposta %d", opcao);
                                int sorte = rand() % 100;

                                if (nivel <= 200 && sorte < 20) {
                                    printf("\nSom da Natureza: Resolve os teus problemas noutro lugar");
                                    printf("\nSom da Natureza: Num momento para outro uma parede mágica é criada\n");
                                    opcao = 0;
                                    while (opcao == 0 || opcao < 0 || opcao > 2) {
                                        printf("\nQueres insistir?");
                                        printf("\n1-- Sim");
                                        printf("\n2--Não\n");
                                        opcao = leituraint();
                                        if (opcao == 1) {
                                            sorte = rand() % 100;
                                            if (sorte < 90) {
                                                printf("\nAs borboletas rodeiam-te e tu começas a arder até morrer\n");
                                                saopedro();
                                            } else {
                                                printf("\nTás com sorte hoje e uma fada aparece e permite que entres, ganhaste 20 pontos no teu nível\n");
                                                nivel = nivel + 20;
                                                printf("\nEntraste na Floresta Banca e tens algumas missões ou pessoas com quem falar\n");
                                                missaoflorestabranca_2();

                                            }
                                        } else {
                                            mapa();
                                        }
                                    }
                                }
                            } else if (opcao == 5) {
                                if (nivel <= 550) {
                                    printf("\nAs borboletas rodeiam-te e tu começas a arder até morrer\n");
                                    saopedro();
                                }
                                int sorte = rand() % 100;
                                if (nivel >= 550 && sorte < 75) {
                                    sorte = rand() % 3;
                                    if (sorte < 2) {
                                        printf("\nConseguiste furar a parede mágica criada pelas borboletas e entras");
                                        printf("\nEntras pela floresta para a conheceres e sobres o nível em 50\n");
                                        nivel = nivel + 50;
                                        missaoflorestabranca_2();


                                    } else {
                                        printf("\nFicas ferido pelo fogo das borboletas mas consegues fugir, menos 10 pontos de nível\n");
                                        nivel = nivel - 10;
                                        mapa();

                                    }

                                }
                            }
                            else {
                                int sorte = rand() % 100;
                                if (nivel <= 550) {
                                    printf("\nUma barreira mágica é criada e não entras\n");
                                    mapa();
                                } else if (nivel <= 550 && sorte < 10) {
                                    printf("\nTás com sorte hoje e o um corredor mágico é criado, ganhaste 5 pontos no teu nível");
                                    nivel = nivel + 5;
                                    printf("\nEntraste na Floresta Branca e tens algumas missões ou seres com quem falar\n");
                                    missaoflorestabranca_2();
                                } else {
                                    printf("\nQuase morres a tentar passar as borboletas mas foges, menos 10 pontos de nível\n");
                                    nivel = nivel - 10;
                                    mapa();
                                }
                            }
                        }
                    } else if (personagem == 3) {
                        printf("\nHistória por implementar");
                        mapa();

                    }

                }
                break;
                case 2:
                    mapa();
                break;

            }


    }
}

void missaoflorestabranca_2() {
    printf("\nEstás dentro da floresta Branca");
    printf("\nTens 3 tipos de missões aqui");
    printf("\nO teu nível é igual a %d",nivel);
    printf("\nEscolhe bem o que queres, para não vires chorar depois");
    if (personagem == 1) {
        int opcao = 0 ;
        while (opcao == 0 || opcao < 0 || opcao > 4) {
            printf("\n1-- Se queres uma missão fácil");
            printf("\n2-- Se queres uma missão média");
            printf("\n3-- Se queres uma missão díficil");
            printf("\n4-- Se queres ir para o mapa\n");
            opcao = leituraint();
            if (opcao == 1){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                printf("\n1-- Fada Madrinha");
                printf("\n2-- Mulher");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2){
                    printf("\n1-- Fada Madrinha");
                    printf("\n2-- Mulher\n");
                    opcao =leituraint();
                }
                if (opcao ==1){
                    printf("\nFada: Olá bom Homem o que procuras aqui?");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nFada: Não precisas de trabalho");
                    printf("\nHomem: .....porque não?");
                    printf("\nFada: Dá-me um dente teu!");
                    printf("\nTu arrancas um dente e recebes uma moeda e ganhas, 20 pontos de nível\n");
                    nivel = nivel + 20;
                    missaoflorestabranca_2();

                } else{
                    printf("\nMulher: Olá macho o que queres?");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nMulher: Ok, tenho ali roupara para lavar");
                    printf("\nHomem: Eu não lavo roupa");
                    printf("\nMulher: Hm, queres morrer?");
                    printf("\nGanhas 5 pontos de nível porque lavaste a roupa\n");
                    nivel = nivel +5;
                    missaoflorestabranca_2();

                }
            }
            else if (opcao == 2){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma\n");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Jenniifer Lopez");
                    printf("\n2-- David Carreirra\n");
                    opcao = leituraint();
                    if (opcao == 1){
                        printf("\nJLO: Olá novinho tudo bem?");
                        printf("\nHomem: Olá minha rainha");
                        printf("\nJLO: Queres trabalho?");
                        printf("\n Homem: Não eu quero é comer\n");
                        int sorte = rand()%100;
                        if (sorte < 30){
                            printf("\nJLO: Vais é para a cozinha lavar o chão da minha casa");
                            printf("\nLavar o chão  não é fácil ganhas 20 pontos de nível\n");
                            nivel = nivel +20;
                            missaoflorestabranca_2();
                        }
                        else if (sorte < 75 && sorte >= 30){
                            printf("\nEla fazia tempo que não via um Homem e faz sexo cntg");
                            printf("\nGanhas 5 pontos de nível porque nem duraste um min\n");
                            nivel = nivel +5;
                            missaoflorestabranca_2();
                        }
                        else{
                            printf("\nJLO: É o teu dia de sorte hoje faço anos anda comer bolo");
                            printf("\nComes o que queres e sobes 30 pontos de nível\n");
                            nivel = nivel + 30;
                            missaoflorestabranca_2();
                        }
                    }
                    else{
                        printf("\nCarreiira: Olá Homem lindo");
                        printf("\nHomem: Olá , precisas de ajuda?");
                        printf("\nMiúda: Sim, estou muito sozinho");
                        printf("\nHomem: Ok eu faço-te companhia\n");
                        int sorte = rand()%100;
                        if (sorte < 40){
                            printf("\nFicam a tocar a cantar má música a noite toda");
                            printf("\nEle ri-se e vai-se embora");
                            printf("\nO que conta é a intenção ganhas 10 pontos de nível\n");
                            nivel = nivel +10;
                            missaoflorestabranca_2();
                        } else{
                            printf("\nFoste o maior, compões um nova música pimba");
                            if (sorte <20){
                                printf("\nEle é filho do Tony, ganhas reputação e nível e sobes 40 pontos\n");
                                nivel =  nivel + 40;
                                missaoflorestabranca_2();

                            }else if (sorte >= 20 && sorte <= 70){
                                printf("\nEle sorri e dá-te um beijo na cara e sobes 25 pontos de nível\n");
                                nivel = nivel + 25;
                                missaoflorestabranca_2();

                            }else{
                                printf("\nEla olha para ti e dá-te dinheiro para sexo");
                                printf("\nTu recusas porque és, sei lá o quê!");
                                printf("\nSobes 5 pontos de nível\n");
                                nivel = nivel +5;
                                missaoflorestabranca_2();


                            }
                        }
                    }
                }


            }

            else if (opcao==3){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Falar com a Branca de neve");
                    printf("\n2-- Entrar na mina dos 8 anões\n");
                    opcao = leituraint();
                    if (opcao==1){
                        if (nivel < 200){
                            printf("\nA branca de neve está a dormir\n");
                            missaoflorestabranca_2();
                        }else if (nivel >= 200){
                            int sorte = rand()%100;
                            if (sorte <50){
                                printf("\nA bela mulher olha para ti, branca como neve, à espera que fales");
                                printf("\nOlá Beleza das neves, eu estou aqui para trabalhar e ser-lhe útil");
                                printf("\nBranca: Meu filho então limpa-me a casa toda\n");
                                opcao=0;
                                while (opcao == 0 || opcao < 0 || opcao > 2) {
                                    printf("\n1-- Aceitas");
                                    printf("\n2-- Recusas\n");
                                    opcao = leituraint();
                                    if (opcao == 2){
                                        printf("\nRecusar um pedido de tal senhora?!, perdes 20 pontos\n");
                                        nivel = nivel -20;
                                        missaoflorestabranca_2();

                                    }else{
                                        printf("\nTu limpas a casa toda, no fim recebes um diamante e ficas mais forte");
                                        printf("\nMais 80 pontos de nível\n");
                                        nivel = nivel +80;
                                        missaoflorestabranca_2();
                                    }
                                }
                            }
                        }
                    } else{
                        printf("\nEntras na mina dos 8 anões\n");
                        if (nivel <200){
                            printf("\nPéssima ideia, és roubado e ainda levas nos olhos");
                            printf("\nMenos 20 pontos de nível\n");
                            nivel = nivel -20;
                            missaoflorestabranca_2();
                        } else{
                            int sorte = rand()%100;
                            if (sorte < 40){
                                printf("\nPéssima ideia, és apanhado pelos anões e trabalhas até morrer");
                                printf("\nMenos 20 pontos de nível\n");
                                nivel = nivel -20;
                                saopedro();
                            }
                            else{
                                printf("\nAnões: O que tu queres Gigante?");
                                printf("\nPreciso de trabalhar");
                                printf("\nSerás de confiança diz o anão morcão");
                                printf("\nPensei que eram só 7...");
                                printf("\nA nossa mãe ainda é nova e teve esse morcão diz o Zangado");
                                printf("\nTrabalhas uma semanas e és muito bem pago e ficas mais forte");
                                printf("\nGanhas 70 pontos de nível\n");
                                nivel = nivel +70;
                                missaoflorestabranca_2();
                            }
                        }
                    }
                }
            }

        }

    }
}

void missaoporto() {
    printf("\nEstás na entrada da Cidade do Porto");
    printf("\nQueres falar com pessoas ou ir para o mapa?");
    int opcao = 0;

    while (opcao == 0 || opcao < 0 || opcao > 2) {
        printf("\n1-- Se queres falar com pessoas");
        printf("\n2-- Se queres ir para o mapa\n");
        opcao = leituraint();
    }
    switch (opcao) {
        case 1:
            printf("\nEscolheu falar com pessoas!");
            opcao = 0;
            while (opcao == 0 || opcao < 0 || opcao > 2) {
                printf("\nNa entrada da cidade");
                printf("\n1-- Falar com o guarda?");
                printf("\n2-- Desistir e voltar ao mapa?..\n");
                opcao = leituraint();
                if (opcao == 1) {
                    printf("\nO guarda olha para ti sem dizer nada");
                    printf("\nAté que tu decides falar com ele");
                    if (personagem == 1) {
                        if (objectivo == 0) {
                            printf("\nAndré: Olá eu gostaria de entrar na cidade é possível?");
                            printf("\nGuarda: Porque queres entrar Homem?");
                            opcao = 0;
                            while (opcao == 0 || opcao < 0 || opcao > 5) {
                                printf("\nEscolha uma resposta");
                                printf("\n1-- Quero entrar para descansar e trabalhar, sou honesto");
                                printf("\n2-- Eu sou Do Porto quero voltar a ver a minha bela cidade");
                                printf("\n3-- Vim tomar o que é meu por direito");
                                printf("\n4-- Problemas pessoais Sr Guarda.. ");
                                printf("\n5-- Não te interessa deixa-me entrar\n");
                                opcao = leituraint();
                                if (opcao == 1 || opcao == 2) {
                                    printf("\nEscolheste a resposta %d", opcao);
                                    printf("\nO Guarda manda abrir o Portão, ganhas ponto de nível, 10");
                                    printf("\nEntraste na Cidade do Porto e tens algumas missões ou pessoas com quem falar\n");
                                    nivel = nivel + 10;
                                    missaoporto_2();

                                } else if (opcao == 4) {
                                    printf("\nEscolheste a resposta %d", opcao);
                                    int sorte = rand() % 100;

                                    if (nivel <= 200 && sorte < 30) {
                                        printf("\nGuarda: Não quero saber dos teus problemas pessoais para nada");
                                        printf("\nGuarda: Se insistires vais desta para melhor volta para onde vieste");
                                        opcao = 0;
                                        while (opcao == 0 || opcao < 0 || opcao > 2) {
                                            printf("\nQueres insistir?");
                                            printf("\n1-- Sim");
                                            printf("\n2--Não\n");
                                            opcao = leituraint();
                                            if (opcao == 1) {
                                                sorte = rand() % 100;
                                                if (sorte < 90) {
                                                    printf("\nO guarda não achou piada e puxou da arma e deu-te dois tiros na cabeça");
                                                    saopedro();
                                                } else {
                                                    printf("\nTás com sorte hoje e o guarda abre o portão, ganhaste 20 pontos no teu nível");
                                                    nivel = nivel + 20;
                                                    printf("\nEntraste na Cidade do Porto e tens algumas missões ou pessoas com quem falar");
                                                    missaoporto_2();

                                                }
                                            } else {
                                                mapa();
                                            }
                                        }
                                    }else{
                                        printf("\nNão quero saber dos teus problemas diz o guarda e voltas para o mapa\n");
                                        mapa();
                                    }
                                } else if (opcao == 3) {
                                    if (nivel <= 550) {
                                        printf("\nO guarda não achou piada e puxou da arma e deu-te dois tiros na cabeça\n");
                                        saopedro();
                                    }
                                    int sorte = rand() % 100;
                                    if (nivel >= 550 && sorte < 75) {
                                        sorte = rand() % 3;
                                        if (sorte < 2) {
                                            printf("\nMataste o guarda antes de ele reagir");
                                            printf("\nEntras pela cidade para a conquistar\n");
                                            nivel = nivel + 50;
                                            objetivo();


                                        } else {
                                            printf("\nO guarda não cede e nem tu cedes e voltas ao mapa mas com penalização, menos 100 pontos de nível\n");
                                            nivel = nivel - 10;
                                            mapa();

                                        }
                                    }
                                } else {
                                    int sorte = rand() % 100;
                                    if (nivel <= 550) {
                                        printf("\nO guarda não achou piada e puxou da arma e deu-te dois tiros na cabeça\n");
                                        saopedro();
                                    } else if (nivel <= 550 && sorte < 10) {
                                        printf("\nTás com sorte hoje e o guarda abre o portão, ganhaste 5 pontos no teu nível");
                                        nivel = nivel + 5;
                                        printf("\nEntraste na Cidade do Porto e tens algumas missões ou pessoas com quem falar\n");
                                        missaoporto_2();
                                    } else {
                                        printf("\nO guarda não cede e nem tu cedes e voltas ao mapa mas com penalização, menos 10 pontos de nível\n");
                                        nivel = nivel - 10;
                                        mapa();
                                    }
                                }
                            }
                        } else {
                            printf("\nEntraste na cidade como é  obvio, és o Rei disto tudo");
                            printf("\nParece tudo em ordem por estes lados\n");
                            mapa();
                        }
                    }  else if (personagem == 3) {
                        printf("\nHistória por implementar");
                        mapa();


                    }  else {

                    }

                } else {
                    mapa();
                }
            }
            break;
        case 2:
            mapa();
            break;

    }

}

void missaoporto_2() {
    printf("\nEstás dentro da cidade do Porto");
    printf("\nTens 3 tipos de missões aqui");
    printf("\nO teu nível é igual a %d",nivel);
    printf("\nEscolhe bem o que queres, para não vires chorar depois");
    if (personagem == 1) {
        int opcao = 0 ;
        while (opcao == 0 || opcao < 0 || opcao > 4) {
            printf("\n1-- Se queres uma missão fácil");
            printf("\n2-- Se queres uma missão média");
            printf("\n3-- Se queres uma missão díficil");
            printf("\n4-- Se queres ir para o mapa\n");
            opcao = leituraint();
            if (opcao == 1) {
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");

                opcao = 0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- O Sérgio");
                    printf("\n2-- O Rocha\n");
                    opcao = leituraint();
                }
                if (opcao == 1) {
                    printf("\nSérgio: Ó palhaço o que queres?");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nSérgio: Então começa a correr à volta do campo");
                    printf("\nHomem: .....");
                    printf("\nRápido, antes que leves no fucinhuu!");
                    printf("\nTu corres até caíres para o chão e ganhas, 20 pontos de nível");
                    nivel = nivel + 20;
                    missaoporto_2();

                } else {
                    printf("\nRocha: Olá Home, o que queres c***..pi");
                    printf("\nHomem: Preciso de trabalho");
                    printf("\nRocha: Também eu, desde da m***pi do Covid-19 que não tenho");
                    printf("\nHomem: O que é o Covid?");
                    printf("\nRocha: Olha-me este... vai À esquina a ver se estou lá");
                    printf("\nGanhas 5 pontos de nível, não sei o que esperavas do Rocha mas pronto");
                    nivel = nivel + 5;
                    missaoporto_2();

                }
            }
            else if (opcao == 2){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");

                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- O porteiro do 'A  tua prima'");
                    printf("\n2-- Uma miúda que parece ter uns 19 anos\n");
                    opcao = leituraint();
                    if (opcao == 1){
                        printf("\nPorteiro: Olá meu menino");
                        printf("\nHomem: Olá meu senhor");
                        printf("\nPorteiro: Queres trabalho?");
                        printf("\n Homem: Não eu quero é comer");
                        int sorte = rand()%100;
                        if (sorte < 30){
                            printf("\nPorteiro: Vais é para a cozinha lavar pratos e no fim comes os restos");
                            printf("\nLavar pratos e comer restos, não é fácil ganhas 20 pontos de nível");
                            nivel = nivel +20;
                            missaoporto_2();
                        }
                        else if (sorte < 75 && sorte >= 30){
                            printf("\nO porteiro dá-te um chuto no traseiro porque não quer gajos como tu lá dentro");
                            printf("\nGanhas 5 pontos de nível porque ficaste mais rijo");
                            nivel = nivel +5;
                            missaoporto_2();
                        }
                        else{
                            printf("\nPorteiro: É o teu dia de sorte és o nosso centésimo cliente");
                            printf("\nComes o que queres e sobes 30 pontos de nível");
                            nivel = nivel + 30;
                            missaoporto_2();
                        }
                    }
                    else{
                        printf("\nMiúda: Olá Homem Grande e feio");
                        printf("\nHomem: Olá Bonita, precisas de ajuda?");
                        printf("\nMiúda: Sim, o meu gato está em cima daquela árvore assustado");
                        printf("\nHomem: Ok vou buscar o teu gato");
                        int sorte = rand()%100;
                        if (sorte < 40){
                            printf("\nCaíste da árvore e gato saltou sozinho para o colo da miúda");
                            printf("\nEla ri-se e vai-se embora");
                            printf("\nO que conta é a intenção ganhas 10 pontos de nível");
                            nivel = nivel +10;
                            missaoporto_2();
                        } else{
                            printf("\nFoste o maior, salvas o gato e entregas às mãos da miúda");
                            if (sorte <20){
                                printf("\nEla é filha do Papa, ganhas reputação e nível e sobes 40 pontos");
                                nivel =  nivel + 40;
                                missaoporto_2();

                            }else if (sorte >= 20 && sorte <= 70){
                                printf("\nEla sorri e dá-te um beijo na cara e sobes 25 pontos de nível");
                                nivel = nivel + 25;
                                missaoporto_2();

                            }else{
                                printf("\nEla olha para ti e dá-te dinheiro como recompensa");
                                printf("\nTu recusas porque és burro");
                                printf("\nSobes 5 pontos de nível");
                                nivel = nivel +5;
                                missaoporto_2();


                            }
                        }
                    }
                }


                }

            else if (opcao==3){
                printf("\nBem-vindo Homem");
                printf("\nTens duas pessoas com quem falar, escolhe uma");
                opcao=0;
                while (opcao == 0 || opcao < 0 || opcao > 2) {
                    printf("\n1-- Falar com o Papa");
                    printf("\n2-- Entrar no Porto de Leixões\n");
                    opcao = leituraint();
                    if (opcao==1){
                        if (nivel < 200){
                        printf("\nO Papa Pinto Da C tem mais que fazer do que ouvir um Zé Ninguém");
                        missaoporto_2();
                        }else if (nivel >= 200){
                            int sorte = rand()%100;
                            if (sorte <50){
                                printf("\nO Papa Pinto da C olha para ti com desprezo, à espera que fales");
                                printf("\nOlá iminência, eu estou aqui para trabalhar e ser-lhe útil");
                                printf("\nPapa: Meu filho então limpa-me o Estádio do Dragão todo");
                                opcao=0;
                                while (opcao == 0 || opcao < 0 || opcao > 2) {
                                    printf("\n1-- Aceitas");
                                    printf("\n2-- Recusas\n");
                                    opcao = leituraint();
                                    if (opcao == 2){
                                        printf("\nNinguém recusa nada ao Papa, perdes 20 pontos");
                                        nivel = nivel -20;
                                        missaoporto_2();

                                    }else{
                                        printf("\nTu limpas o Dragão todo, no fim és bem pago e ficas mais forte");
                                        printf("\nMais 80 pontos de nível");
                                        nivel = nivel +80;
                                        missaoporto_2();
                                    }
                            }
                        }
                    }
                } else{
                        printf("\nEntras no Porto de Leixões");
                        if (nivel <200){
                            printf("\nPéssima ideia, és roubado e ainda levas nos olhos");
                            printf("\nMenos 20 pontos de nível");
                            nivel = nivel -20;
                            missaoporto_2();
                        } else{
                            int sorte = rand()%100;
                            if (sorte < 40){
                                printf("\nPéssima ideia, és roubado e ainda levas nos olhos");
                                printf("\nMenos 20 pontos de nível");
                                nivel = nivel -20;
                                missaoporto_2();
                            }
                            else{
                                printf("\nArranjas trabalho como estivador, és bem pago porque em Lisboa tavam em greve");
                                printf("\nGanhas 70 pontos de nível");
                                nivel = nivel +70;
                                missaoporto_2();
                            }
                        }
                    }
            }
        }

    }

    }
}

void objetivo() {
    int opcao = 0;
    if (personagem == 1) {
        printf("\nEstás poderoso e pronto para conquistar a cidade do Porto o teu grande objectivo como Homem");
        printf("\nMas se apenas queres fazer uma pequenas missões a escolha é tua");


        while (opcao == 0 || opcao < 0 || opcao > 2) {
            printf("\n1-- Se queres fazer umas pequenas missões");
            printf("\n2-- Se queres tentar a conquista e a glória\n");
            opcao = leituraint();
            if (opcao == 1) {
                missaoporto_2();
            } else {
                printf("\nCoragem não te falta, o exercíto do Porto com o Pinto da Costa VI como General");
                printf("\nEsperam por ti, as chances estão contra ti, mas tu és quase um Deus\n");
                int sorte = rand() % 100;
                if (sorte < 20) {
                    printf("\nNão consegues com eles todos mas consegues fugir sem consequências\n");
                    mapa();
                } else if (sorte < 40 && sorte >= 20) {
                    printf("\nFicaste ferido e quase morrias mas consegues fugir, perdes 500 pontos de nível\n");
                    nivel = nivel - 500;

                } else if (sorte < 60 && sorte >= 40) {
                    printf("\nTemos pena mas até os deuses morrem mas talvez não seja o fim, já que morreste a lutar\n");
                    saopedro();
                } else {
                    printf("\nÉs o novo rei do Porto, mataste todos os que se opunham a ti na cidade\n");
                    objectivo = 1;
                    nivel = nivel + 10000;
                    printf("\nA tua força tornou-se mitíca e a Cidade é tua, entras e sais quando queres\n");
                    mapa();

                }
            }
        }
    }else if (personagem==3){
        printf("\nAinda não se sabe os seus objetivos\n");
        mapa();
    }
}


int escolhapersonagem() {
    int i = 0;
    while (i == 0 || i < 0 || i > 3) {
        printf("\n\nEscolha uma das personagens abaixo\n");
        printf("1-- HOMEM\n");
        printf("2-- MULHER\n");
        printf("3-- VELHOTE\n");

        i = leituraint();

    }

    switch (i) {

        case 1:
            //homem
            printf("Escolheu o Homem vamos lá contar a sua história\n\n");
            int enter = 1;
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }

            printf("O André Ricardo é um pequeno Homem, pouco inteligente que tem apenas um objectivo em mente que é encontrar\n o amor da vida dele e conquistar um Castelo, vive no Porto, estudou mas não sabe fazer nada útil,\n mas é bom a lutar com as mãos e tem um problema não tem medo de morrer e não acredita em Deus, citando o próprio:\n“MORRER HOJE OU DAQUI A 50 ANOS É IGUAL JÁ QUE DEPOIS DE MORRER ACABA TUDO”\n");
            enter = "";
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }

            printf("Características:\n");
            printf("Forte\nLento\nVisão Fraca\nPouco Inteligente\nQuase Racional\nBonito\nBom\n");
            enter = "";
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }


            return 1;

        case 2:
            //mulher
            /*printf("Escolheu a Mulher vamos lá contar a sua história\n\n");
            enter = 1;
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }

            printf("O André Ricardo é um pequeno Homem, pouco inteligente que tem apenas um objectivo em mente que é encontrar\n o amor da vida dele e conquistar um Castelo, vive no Porto, estudou mas não sabe fazer nada útil,\n mas é bom a lutar com as mãos e tem um problema não tem medo de morrer e não acredita em Deus, citando o próprio:\n“MORRER HOJE OU DAQUI A 50 ANOS É IGUAL JÁ QUE DEPOIS DE MORRER ACABA TUDO”\n");
            enter = "";
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }

            printf("Características:\n");
            printf("Forte\nLento\nVisão Fraca\nPouco Inteligente\nQuase Racional\nBonito\nBom\n");
            enter = "";
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }
            return 2;
*/
        case 3:
            //velhote
            printf("Escolheu o Velhote vamos lá contar a sua história\n\n");
            enter = 1;
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }

            printf("Velhote sem nome, há quem pense que é um antigo Deus Nórdico, uns dizem\n"
                   "que poderá ser Thor ou Odin, os avós dos avós dos jovens já conheciam este Velhote.\n"
                   "Há histórias em que ele invoca tempestades e raios, em que ele combate e nunca\n"
                   "morre. Os seu objetivos e propósitos são dúbios, ou seja, pouco claros, mas há quem\n"
                   "diga que ele tenta voltar para junto dos Deuses na Silver City. Alguns dizem que é no\n"
                   "paraíso.”\n");
            enter = "";
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }

            printf("Características:\n");
            printf("Fraco(aparenta)\nLento(aparenta)\nSuper Inteligente\nSuper Experiente\nPoderes Divinos\nPacífico\nTeimoso\n");
            enter = "";
            while (enter != 0) {
                printf("Escreva 0/zero para continuar\n");
                enter = leituraint();

            }
            return 3;




    }



}


void intro() {
    printf("\nDaqui a dois séculos o mundo está bem diferente... entre guerras e mudanças climatéricas");
    printf("\nO mundo real e o mundo fantástico colidiram e criaram uma nova realidade mista");
    printf("\nCertos países evoluiram e conquistaram outros, por exemplo Marrocos conquistou Portugal");
    printf("\nDrácula domina o Leste Europeu e a maioria da população mundial morreu");
    printf("\nSobraram pouco mais de 100 milhões de pessoas no planeta e mais uns milhões de monstros");
    printf("\nAlgumas cidades sobreviveram como por exemplo a cidade do Porto");
    printf("\nNo meio desta confusão tens que tentar surviver e atingir os teus objectos");
    printf("\nou pelo menos não morrer... ");
    printf("\ne teres o azar de acabar no inferno\n");
    int enter=1;
    while (enter != 0) {
        printf("\nEscreva 0/zero para continuar\n");
        enter = leituraint();

    }
}

void salvarjogo() {
        FILE *gravar;

        gravar = fopen("gravar.txt", "w");

    if (gravar != NULL){
        printf("Ficheiro criado com sucesso\n");
    } else{
        printf("Falhou a criação do ficheiro\n");

    }
    printf("\nVamos analisar e gravar os dados\n");
    putw (nivel, gravar);
    putw(personagem,gravar);
    putw(objectivo,gravar);


    fclose(gravar);
    gravar = fopen("gravar.txt", "r");
    printf("Nivel,Personagem,objectivo\n"); //apenas para listar a gravação dos três parametros
    while (getw(gravar) != EOF){
        printf("%d\n");
    }
    fclose(gravar);

    mapa();

}

int leituraint(void) {
    int inteiro;
    scanf("%d", &inteiro);
    return inteiro;
}

/*float leiturafloat(void) {
    float floatt;
    scanf("%f", &floatt);
    return floatt;
}

char leiturachar(void) {
    char carater;
    scanf("%c", &carater);
    return carater;
}*/
